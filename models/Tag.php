<?php namespace trka\Taggable\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'trka_taggable_tags';

    /**
     * @var array allow mass assignment
     */
    protected $fillable = [
        'label',
        'slug'
    ];

    protected $slugs = [
        'slug' => 'label'
    ];

    protected function beforeSave()
    {
        if (!$this->slug) {
            $this->slugAttributes();
        }
    }
}
