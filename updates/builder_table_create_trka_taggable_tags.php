<?php namespace trka\Taggable\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTrkaTaggableTags extends Migration
{
    public function up()
    {
        Schema::create('trka_taggable_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('label', 128);
            $table->string('slug', 128);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('trka_taggable_tags');
    }
}
