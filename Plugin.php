<?php namespace trka\Taggable;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    const GLOBAL_RELATION_PROP_TAG = 'globalTags';
    const GLOBAL_RELATION_PROP_CATEGORY = 'globalCategories';

    public function registerComponents()
    {
        return [
            \trka\Taggable\Components\GlobalRelationships::class => 'globalRelationships',
            \trka\Taggable\Components\GlobalTagsList::class => 'globalTagsList',
        ];
    }

    public function registerSettings()
    {
    }
}
