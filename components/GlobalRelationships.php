<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 7/15/18
 * Time: 10:00 AM
 */

namespace trka\Taggable\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Support\Collection;
use trka\Taggable\Models\Tag;
use trka\Taggable\Plugin;

class GlobalRelationships extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Global Relationships',
            'description' => 'Find other entities with similar global tags/categories'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
    }

    /**
     * @param $taggableEntity
     */
    public function parse($taggableEntity)
    {
        $relations = [];
        if (!$taggableEntity->globalTags) {
            return $relations;
        }

        //-- populate the relationships arrays
        foreach ($taggableEntity->globalTags as $t) {
            foreach ($t->belongsToMany as $relationType => $params) {
                foreach ($t[$relationType] as $related) {
                    if (!isset($relations[$relationType])) {
                        $relations[$relationType] = [];
                    }
                    if (!isset($relations[$relationType][$related->id])) {
                        $relations[$relationType][$related->id] = $related;
                    }
                }
            }
        }

        //-- convert to 'honest' collections
        foreach ($relations as $k => $v) {
            $relations[$k] = new Collection($v);
        }

        return $relations;
    }
}