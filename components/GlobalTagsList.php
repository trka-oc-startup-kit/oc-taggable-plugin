<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 7/28/18
 * Time: 9:52 AM
 */

namespace trka\Taggable\Components;


use Cms\Classes\ComponentBase;
use trka\Taggable\Models\Tag;

class GlobalTagsList extends ComponentBase
{
    public $tags;

    public function componentDetails()
    {
        return  [
          'name' => 'Global Tags List',
          'description' => 'Lists all available global tags'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->tags = Tag::all();
    }

}